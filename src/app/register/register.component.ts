import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });

//  
//
//   
//   bros:any=
//   
//   mbs:any=[0,1,2,3,4,5,6,7,8,9]
//  mss:any=[0,1,2,3,4,5,6,7,8,9]
//   ste:any=[{name:"Andrapradesh"},{name:"Others"}]
//   country:any=[{name:"india"},{name:"others"}]
profilefor: any = [
  {value: 'Self', viewValue: 'Self'},
  {value: 'Parent', viewValue: 'Parent'},
  {value: 'Sibling', viewValue: 'Sibling'}, {value: 'Realitive', viewValue: 'Realitive'}, {value: 'Friends', viewValue: 'Friends'},
];
mariticalstatus:any = [{value: 'Un Married', viewValue: 'Un Married'},{value: 'Widow', viewValue: 'Widow'},
{value: 'Divorced', viewValue: 'Divorced'},{value: 'Saparated', viewValue: 'Saparated'}]
caste:any = [{value: 'select', },{value: 'Blacksmith'},
{value: 'Carpenters'},{value: 'Goldsmith'},
{value: 'Sculptors' },{value: 'Others'}]
mothertonge:any=[{value: 'select', },{value: 'Telugu'},
{value: 'Hindhi'},{value: 'English'},{value: 'Others'}]
fooodtype:any=[{value: 'select', },{value: 'veg'},{value: 'nonveg'},{value: 'both'},{value: 'Others'}]
countrylivingin:any = [{value: 'select' },{value: 'India'},{value: 'others'}]
gender:any = [{value: 'select' },{value: 'male'},{value: 'female'},{value: 'Others'}]
star:any = [{value:'Select'},{value:'Aswini'},{value:'Bharani'},{value:'Kruthika'},{value:'Rohini'},
{value:'Mrigasira'},{value:'Punarvasu'},{value:'Arudra'},{value:'Pushyami'},
{value:'Aslesha'},{value:'Magha'},{value:'Poorva Phalguni'},{value:'Chitta'},{value:'Uttara Phalguni'},
{value:'Swathi'},{value:'Visakha'},{value:'Anuradha'},{value:'Jesta'},{value:'Moola'},
{value:'Hastha'},{value:'Poorvashada'},{value:'Uttarashada'},{value:'Sravana'},{value:'Dhanishta'},{value:'ravathi'},]
rasi:any = [{value:'Select'},{value:'Dhanu (Sagittarius)'},{value:'Kanya (Virgo)'},{value:'Karkataka (Cancer)'},
{value:'Kumbha (Aquarius)'},{value:'Makara (Capricorn)'},{value:'Meena  (Pisces)'},{value:'Mesha (Aries)'},
{value:'Simha (Leo)'},{value:'Thula  (Libra)'},{value:'Midhuna (Gemini)'},{value:'Vruchika'},{value:'Vrushabha (Taurus)'},]
height:any = [{value:'Select'},{value:'122cm - 4ft'},{value:'125cm - 4ft 1in'},{value:'134cm - 4ft 4in'},{value:'134cm - 4ft 5in'},
              {value:'137cm - 4ft 6in'},{value:'139cm - 4ft 7in'},{value:'142cm - 4ft 8in'},{value:'144cm - 4ft 9in'},
              {value:'147cm - 4ft 10in'},{value:'Sel149cm - 4ft 11inect'},{value:'152cm - 5ft'},{value:'"154cm - 5ft 1in'},
              {value:'157cm - 5ft 2in'},{value:'160cm - 5ft 3in'},{value:'162cm - 5ft 4in'},{value:'162cm - 5ft 5in'},
              {value:'162cm - 5ft 6in'},{value:'162cm - 5ft 7in'},{value:'162cm - 5ft 8in'},{value:'162cm - 5ft 9in'},
              {value:'162cm - 5ft 10in'},{value:'162cm - 5ft 11in'},{value:'182cm - 6ft'},{value:'185cm - 6ft 1in'},
              {value:'185cm - 6ft 2in'},{value:'185cm - 6ft 3in'},{value:'185cm - 6ft 4in'},{value:'185cm - 6ft 5in'},
              {value:'185cm - 6ft 6in'},{value:'185cm - 6ft 7in'},{value:'185cm - 6ft 8in'},{value:'185cm - 6ft 9in'},
              {value:'185cm - 6ft 10in'},{value:'185cm - 6ft 11in'},{value:'"213cm - 7ft'},
             ]
 Complextion:any = [{value:'Select'},{value:'Dark'},{value:'Fair'},{value:'Very Fair'},
                    {value:'Wheatish'},{value:'SeWheatish Brownlect'},{value:'Wheatish Medium'}]
  Brothers :any = [0,1,2,3,4,5,6,7,8,9]  
  Sisters:any=[0,1,2,3,4,5,6,7,8,9] 
  marriedbros:any = [0,1,2,3,4,5,6,7,8,9]  
  marriedsis:any = [0,1,2,3,4,5,6,7,8,9] 
  State:any = [{value:'Andhra Pradesh'},{value:'Others'}] 
  Country:any = [{value:'India'},{value:'Others'}] 
  
  firstname:string = "";
  Surname:string = "";
  Email:string = "";
  Password:string = "";
  Mobile:string = "";
  Timeofbirth:string ="";
  placeofbirth:string = "";
  Padam:string="";
  kajuDosam:string = "";
  Gothram:string = "";
  Weight:string = "";
  Education:string = "";
  Occupation:string = "";
  Salary:string = "";
  workingIn:string = "";
  AncestralOrigin:string = "";
  Fname:string = "";
  FatherOcc:string = "";
  Mname:string = "";
  MotherOccu:string = "";
  Adresss:string = "";
  Village:string = "";
  City:string = "";
  Reference:string = "";
  AlternateMobile:string = "";
  Familydetails:string = "";
  Property:string = "";
  Partnerpree:string = "";
  toastrService: any;

constructor(private _formBuilder: FormBuilder ,
           private registerService:  RegisterService,
           private router: Router, ) { }
  

  ngOnInit(): void {
  }
  changeClient(value:any) {
    console.log(value);
}
  changegen(value:any){
    
    console.log(value);
  }
  changeStatus(value:any){
    console.log(value);
  }
  changeCaste(value:any){
    console.log(value);
  }
  changefood(value:any){
    console.log(value);
  }
  changeMothertongue(value:any){
    console.log(value);
  }
  changeCounliving(value:any){
    console.log(value);
  }
  changeStar(value:any){
    console.log(value);
  }
  changeRasi(value:any){
    console.log(value);
  }
  changeHeight(value:any){
    console.log(value);
  }
  changeComplextion(value:any){
    console.log(value);
  }
  changeBrothers(value:any){
    console.log(value);
  }
  changeSisters(value:any){
    console.log(value);
  }
  changeMbrothers(value:any){
    console.log(value);
  }
  changeMsisters(value:any){
    console.log(value);
  }
  changeState(value:any){
    console.log(value);
  }
  changeCountry(value:any){
    console.log(value);
  }
  

  register(){

        let reqdata = {
          Profilecreatedby :this.profilefor,
          Firstname:this.firstname,
          Surname:this.Surname,
          Gender:this.gender,
          Email:this.Email,
          Password:this.Password,
          Mobilenumber:this.Mobile,
          MariticalStatus:this.mariticalstatus,
          Subcaste:this.caste,
          MotherTongue:this.mothertonge,
          countrylivingin :this.countrylivingin,
          Timeofbirth: this.Timeofbirth,
          placeofbirth:this.placeofbirth,
          Star:this.star,
          Padam:this.Padam,
          Rasi:this.rasi,
          KajuDosam:this.kajuDosam,
          Gothram:this.Gothram,
          Height:this.height,
          Weight:this.Weight,
          Complextion: this.Complextion,
          Education: this.Education,
          Occupation:this.Occupation,
          workingIn: this.workingIn,
          Salaryperannum:this.Salary,
          AncestralOrigin:this.AncestralOrigin,
          Fathesname:this.Fname,
          FatherOccupation:this.FatherOcc,
          Mothername:this.Mname,
          MothersOccupation:this.MotherOccu,
          Brothers: this.Brothers,
          Sisters:this.Sisters,
          marriedbrothers: this.marriedbros,
          marriedsisters:this.marriedsis,
          Adresss:this.Adresss,
          Village:this.Village,
          City:this.City,
          State:this.State,
          Country: this.Country,
          Reference: this.Reference,
          AlternateMobile: this.AlternateMobile,
          Familydetails: this.Familydetails,
          Propertydetails: this.Property,
          Partnerpreference:this.Partnerpree,

         
        }
    
        console.log(reqdata)

      //   this.registerService.userregister(reqdata).subscribe((data:any) =>{
      //     this.router.navigate(["/login"])
      //   }
      // )
  
  }
  
  }


function value(value: any) {
  throw new Error('Function not implemented.');
}

