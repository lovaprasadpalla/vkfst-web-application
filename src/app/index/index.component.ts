import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  imgCollection: Array<object> = [
    {
      image: '"../../assets/Find Your Life Partner - VKFST_files/1665327752098-IMG-20221009-WA0003.jpg',
      thumbImage: '"../../assets/Find Your Life Partner - VKFST_files/1665327752098-IMG-20221009-WA0003.jpg',
      alt: 'Image 1',
      title: 'Image 1',
      
    }, {
      image: '../../assets/Find Your Life Partner - VKFST_files/1665340374578-C76E7665-095D-491E-80F1-2C892D1B32C1.jpeg',
      thumbImage: '../../assets/Find Your Life Partner - VKFST_files/1665340374578-C76E7665-095D-491E-80F1-2C892D1B32C1.jpeg',
      title: 'Image 2',
      alt: 'Image 2'
    }, {
      image: '../../assets/Find Your Life Partner - VKFST_files/1665370981735-IMG-20221010-WA0006.jpg',
      thumbImage: '../../assets/Find Your Life Partner - VKFST_files/1665370981735-IMG-20221010-WA0006.jpg',
      title: 'Image 3',
      alt: 'Image 3'

    }, {
      image: '../../assets/Find Your Life Partner - VKFST_files/1665427339502-8AF033B9-620B-4A5E-A100-E0FF340C74DB.jpeg',
      thumbImage: '../../assets/Find Your Life Partner - VKFST_files/1665427339502-8AF033B9-620B-4A5E-A100-E0FF340C74DB.jpeg',
      title: 'Image 4',
      alt: 'Image 4'
    }, {
      image: '../../assets/Find Your Life Partner - VKFST_files/1665455335613-IMG-20221011-WA0000.jpg',
      thumbImage: '../../assets/Find Your Life Partner - VKFST_files/1665455335613-IMG-20221011-WA0000.jpg',
      title: 'Image 5',
      alt: 'Image 5'
    }, {
      image: '../../assets/Find Your Life Partner - VKFST_files/1665456812807-IMG-20221011-WA0002.jpg',
      thumbImage: '../../assets/Find Your Life Partner - VKFST_files/1665456812807-IMG-20221011-WA0002.jpg',
      title: 'Image 6',
      alt: 'Image 6'
    },
    {
      image: '../../assets/Find Your Life Partner - VKFST_files/1665471299759-IMG-20221011-WA0005.jpg',
      thumbImage: '../../assets/Find Your Life Partner - VKFST_files/1665471299759-IMG-20221011-WA0005.jpg',
      alt: 'Image 7',
      title: 'Image 7',
      
    }, {
      image: '../../assets/Find Your Life Partner - VKFST_files/1665631816134-IMG-20221013-WA0004.jpg',
      thumbImage: '../../assets/Find Your Life Partner - VKFST_files/1665631816134-IMG-20221013-WA0004.jpg',
      title: 'Image 8',
      alt: 'Image 8'
    },
];
}


