import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { PraticeComponent } from './pratice/pratice.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PresidentmsgComponent } from './presidentmsg/presidentmsg.component';
import { ContactComponent } from './contact/contact.component';
import { IndexComponent } from './index/index.component';
import { ServicesComponent } from './services/services.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { Header1Component } from './header1/header1.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { NewmatchesComponent } from './newmatches/newmatches.component';

const routes: Routes = [
  { path:'header',component:HeaderComponent },
  {path:'pratice',component:PraticeComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'president',component:PresidentmsgComponent},
  {path:'contact',component:ContactComponent},
  {path:'index',component:IndexComponent},
  {path:'services',component:ServicesComponent},
  {path:'about',component:AboutComponent},
  {path:'home',component:HomeComponent},
  {path:'header1',component:Header1Component},
  {path:'myprofile',component:MyprofileComponent},
  { path:'newmatches',component:NewmatchesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
