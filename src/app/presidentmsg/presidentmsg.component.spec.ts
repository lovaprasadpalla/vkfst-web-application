import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresidentmsgComponent } from './presidentmsg.component';

describe('PresidentmsgComponent', () => {
  let component: PresidentmsgComponent;
  let fixture: ComponentFixture<PresidentmsgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresidentmsgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PresidentmsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
